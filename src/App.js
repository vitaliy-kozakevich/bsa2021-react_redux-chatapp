import React from 'react'
import { createStore, applyMiddleware, compose } from 'redux'
import { Provider } from 'react-redux'
import reduxThunk from 'redux-thunk'
import rootReducer from './redux/rootReducer'
import Chat from './pages/Chat/Chat'
import "materialize-css"

const composeEnhancers =
    typeof window === 'object' &&
    window.__REDUX_DEVTOOLS_EXTENSION_COMPOSE__ ?
    window.__REDUX_DEVTOOLS_EXTENSION_COMPOSE__({
        // Specify extension’s options like name, actionsBlacklist, actionsCreators, serialize...
    }) : compose

const store = createStore(rootReducer, composeEnhancers(applyMiddleware(reduxThunk)))

function App() {
    return (
        <Provider store={store}>
            <Chat />
        </Provider>
    );
}

export default App;
