import { FETCH_MESSAGES, ADD_MESSAGE, DELETE_MESSAGE, EDIT_MESSAGE } from '../actions/actionTypes';

const initialState = {
    messages: []
}

export default function messages(state = initialState, action) {
    switch(action.type) {
        case FETCH_MESSAGES:
            return {
                ...state,
                messages: [
                    ...state.messages,
                    ...action.payload
                ]
            }
        case ADD_MESSAGE:
            return {
                ...state,
                messages: [
                    ...state.messages,
                    action.payload
                ]
            }
        case DELETE_MESSAGE: 
            return {
                ...state,
                messages: state.messages.filter(m => m.id !== action.payload)
            }
        case EDIT_MESSAGE:
            const { id, text } = action.payload
            console.log(id, text);
            const newMessages = state.messages.map(m => {
                if (m.id === id) m.text = text
                return m
            })
            return {
                ...state,
                messages: newMessages
            }
        default:
            return state
    }
}