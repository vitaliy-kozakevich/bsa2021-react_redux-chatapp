import { 
    FETCH_MESSAGES, 
    ADD_MESSAGE,
    EDIT_MESSAGE,
    DELETE_MESSAGE
} from './actionTypes';
import {
    setLoading,
    setError
} from './chat'
import { humanReadableIdOffset } from '../../config'


function setMessages(data) {
    return {
        type: FETCH_MESSAGES,
        payload: data
    }
}

function addNewMessage(data) {
    return {
        type: ADD_MESSAGE,
        payload: data
    }
}

export function deleteMessage(id) {
    return {
        type: DELETE_MESSAGE,
        payload: id
    }
}

export function editMessage(id, text) {
    return {
        type: EDIT_MESSAGE,
        payload: {
            id,
            text
        }
    }
}

export function fetchMessages() {
    return async dispatch => {
        dispatch(setLoading(true))
        try {
            const response = await fetch('https://edikdolynskyi.github.io/react_sources/messages.json')
            const data = await response.json()

            if (!response.ok) {
                throw new Error(data.message || 'Something went wrong!');
            }

            dispatch(setLoading(false))

            dispatch(setMessages(data))
        } catch (error) {
            dispatch(setLoading(false))
            dispatch(setError(error.message))
        } 
    }
}

export function addMessage(text, user) {
    const newMessage = {
        ...user,
        text,
        id: new Date().getTime() - humanReadableIdOffset,
        createdAt: new Date().toISOString(),
        editedAt: ''
    }
    return addNewMessage(newMessage)
}