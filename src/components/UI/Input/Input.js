import React from 'react'
import './Input.scss'

export default function Input({ placeholder, changeValueHandler, value }) {
    return (
        <input 
            className='Input'
            placeholder={placeholder} 
            onChange={changeValueHandler}
            value={value}
        />
    )
}
