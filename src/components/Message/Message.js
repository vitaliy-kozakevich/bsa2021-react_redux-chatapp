import React, { useState } from 'react'
import { connect } from 'react-redux'
import {
    deleteMessage,
} from '../../redux/actions/messages'
import Button from '../UI/Button/Button'
import Modal from '../UI/Modal/Modal'
import './Message.scss'

function Message({ 
    id, 
    avatar, 
    text, 
    user: user_name, 
    createdAt: date, 
    userId, 
    activeUser,
    deleteMessageHandler
}) {
    const [isModalActive, setModalActive] = useState(false)
    const parsedDate = new Date(date).toDateString()
    const isActiveUser = userId === activeUser.userId

    return (
        <div className={isActiveUser ? 'Message flex-end column' : 'Message'}>
            <div className={isActiveUser ? 'Message__wrap width-50' : 'Message__wrap'}>
                <div className='Message__info primary'>
                    <span className='Message__info-name'>{user_name}</span>
                    {
                        !isActiveUser ? 
                            <img className='Message__info-avatar' src={avatar} alt='user_avatar'/> :
                            null
                    }
                </div>
                <div className='Message__info secondary'>
                    <span className='Message__info-text'>{text}</span>
                    <span className='Message__info-date'>{parsedDate}</span>
                </div>
                
            </div>
            {
                !isActiveUser ? (
                    <div style={{ minWidth: '20px'}}>
                        {/* <img src={'../../assets/like.svg'} style={{ backgroundColor: 'lightblue' }}/> */}
                    </div>
                ) : null       
            }
            {
                isActiveUser ? (
                    <div className='Message__btns'>
                        <div className='Message__btns__btn'>
                            <Button clickHandler={() => deleteMessageHandler(id)} name={'Delete'} cls={'error'} />
                        </div>
                        <div className='Message__btns__btn'>
                            <Button clickHandler={() => setModalActive(true)} name={'Edit'} cls={'warn'} />
                        </div>
                    </div>
                ) : null    
            }
            {
                isModalActive ? (
                    <Modal 
                        messageId={id}
                        text={text}  
                        placeholder={'Type text'} 
                        setModalActive={setModalActive} 
                    />
                ) : null
            }
        </div>
    )
}

function mapStateToProps(state) {
    return {
        activeUser: state.chat.user
    }
}


function mapDispatchToProps(dispatch) {
    return {
        deleteMessageHandler: id => dispatch(deleteMessage(id)),
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(Message)
