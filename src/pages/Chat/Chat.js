import React, { useState, useEffect } from 'react'
import { connect } from 'react-redux'
import { addMessage, fetchMessages } from '../../redux/actions/messages'
import { ChatContext } from '../../components/context/Chat.context'
import { useMessage } from '../../components/hooks/message.hook'
import Header from '../../components/Header/Header'
import InputGroup from '../../components/InputGroup/InputGroup'
import MessageList from '../../components/MessageList/MessageList'
import { Wrapper } from '../../Layout/Wrapper'


function Chat({ 
    loading, 
    error,
    user, 
    fetchMessages,
    sendMessage
}) {
    const [inputValue, setInputValue] = useState('')
    const message = useMessage()

    useEffect(() => {
        fetchMessages()
    }, [])

    useEffect(() => {
        message(error)
    }, [error, message])
    
    useEffect(() => {
        window.M.updateTextFields()
    }, [])

    const inputValueHandler = (e) => {
        setInputValue(e.target.value)
    }

    const sendMessageHandler = () => {
        if (!inputValue) {
            message('Type your message first')
            return
        }
        sendMessage(inputValue, user)
        setInputValue('')
    }

    if (loading) {
        return (
            <div style={{ textAlign: 'center', marginTop: '20%' }}>
                <img src={'../../assets/spinner.svg'} />
            </div>
        )
    }

    return (
        <div>
            <Wrapper>
                <ChatContext.Provider value={{ inputValue, inputValueHandler, sendMessageHandler }}>
                    <Header />
                    <MessageList /> 
                    <InputGroup />
                </ChatContext.Provider>
            </Wrapper>
        </div>
    )
}


function mapStateToProps(state) {
    return {
        messages: state.messages.messages,
        loading: state.chat.loading,
        error: state.chat.error,
        user: state.chat.user
    }
}
  
function mapDispatchToProps(dispatch) {
    return {
        fetchMessages: () => dispatch(fetchMessages()),
        sendMessage: (text, user) => dispatch(addMessage(text, user))
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(Chat)
